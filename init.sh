#!/bin/bashrc

yum -y install wget emacs
cat ./emacs > ~/.emacs
mkdir ~/.emacs.d
git clone https://github.com/adolfosousa/ruby-block.el.git ~/.emacs.d/ruby-block
git clone https://github.com/fxbois/web-mode.git ~/.emacs.d/web-mode
git clone https://github.com/yoshiki/yaml-mode.git ~/.emacs.d/yaml-mode