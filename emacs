(column-number-mode t);;カラム番号表示
(line-number-mode t);;行番号表示

(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8)
(setq coding-system-for-write 'utf-8)

(global-set-key "\C-h" 'delete-backward-char);;BSを使えるようにする
(setq kill-whole-line t);; C-k で改行を含め削除
(setq enable-double-n-syntax t);; "nn"と打って、'ん'と変換
(global-set-key "\C-xj" 'goto-line);; goto-line
(global-set-key "\C-xr" 'replace-string);; replace-string

(add-hook 'before-save-hook 'delete-trailing-whitespace);; 空白削除
(setq-default tab-width 2 indent-tabs-mode nil);; インデント空白２つ
(setq web-mode-markup-indent-offset 2)
(setq web-mode-code-indent-offset 2)
(global-set-key "\C-m" 'newline-and-indent)

;; ruby-mode
(require 'ruby-mode)
(add-to-list 'auto-mode-alist '("\\.rb$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Capfile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Gemfile$" . ruby-mode))
(add-to-list 'auto-mode-alist '("\\.*rc$" . ruby-mode))
(add-to-list 'auto-mode-alist '("Guardfile$" . ruby-mode))

(let ((default-directory "~/.emacs.d/"))
      (setq load-path (cons default-directory load-path))
            (normal-top-level-add-subdirs-to-load-path))
;;ruby-electric
(add-hook 'ruby-mode-hook
          (lambda ()
            (require 'ruby-electric)
            (ruby-electric-mode t)))
;; ruby-block
(require 'ruby-block)
(ruby-block-mode t)
(setq ruby-block-highlight-toggle t)
;; web-mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '(".erb" . web-mode))
(add-to-list 'auto-mode-alist '(".html" . web-mode))
(add-to-list 'auto-mode-alist '(".js" . web-mode))
(add-to-list 'auto-mode-alist '(".css" . web-mode))
;; yaml-mode
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
;; slim-mode
(require 'slim-mode)
(add-to-list 'auto-mode-alist '("\\.slim$" . slim-mode))
;; coffee-mode
(require 'coffee-mode)
(add-to-list 'auto-mode-alist '("\\.coffee$" . coffee-mode))