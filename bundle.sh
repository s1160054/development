#!bin/bash

rbenv exec gem install bundler
rbenv rehash
bundle install --path=vendor/bundle

rake db:create
rake db:migrate:reset