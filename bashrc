# .bashrc

export PS1="\w >>"

export DEVELOPER='developer'
export APP='tuberosa'

alias  a="clear && cd /home/$DEVELOPER/$APP/ && git status && git log --oneline -5"
alias  d="clear && cd /home/$DEVELOPER/$APP/ && git status && git log --oneline -5"

alias  p="ps -ef | grep tube && ps -ef | grep ruby"
alias  dd="tail -f /home/$DEVELOPER/$APP/log/development.log"
alias  pp="tail -f /home/$DEVELOPER/$APP/log/production.log"
alias  f='touch /home/$DEVELOPER/$APP/tmp/restart.txt'
alias  ff='service httpd restart'
alias  re='bundle && rake db:migrate:reset && rake db:seed'

alias  g='git log --oneline -20'
alias  s='find * -type f -print | xargs grep $1'

alias  cc='chmod -R 755 ~/ && chown -R $DEVELOPER:wheel ~/'
alias  z='source ~/.bashrc'
alias  zz='emacs ~/.bashrc && z'

alias  ip='wget -q -O - ipcheck.ieserver.net'
alias  iptables='emacs /etc/sysconfig/iptables-config'
alias  sshd='    emacs /etc/ssh/sshd_config'
alias  httpd='  emacs /etc/httpd/conf.d/passenger.conf'
alias  ok='tail -f 50 /etc/httpd/logs/access_log'
alias  er='tail -f 50 /etc/httpd/logs/error_log'
