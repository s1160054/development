install_tools(){
    yum clean all
    yum update  -y
    IFS=$'\n'
    file=(`cat "./tools.txt"`)
    ln=0
    for line in "${file[@]}"; do
        yum -y install $line
    done
    for line in "${file[@]}"; do
        yum list installed | grep $line
    done
}
install_tools