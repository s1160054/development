#!/bin/bashrc

export VERSION='2.2.1'

git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc

source ~/.bashrc
rbenv --version

rbenv install --list
rbenv install $VERSION
rbenv global $VERSION
rbenv version