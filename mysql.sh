export M_IP='160.16.59.80'
export S_IP='153.121.32.181'
export SLAVE='slave_user'
export DB='tuberosa'

# master
mysql -e "FLUSH TABLES WITH READ LOCK;"
mysql -e "GRANT REPLICATION SLAVE ON *.* TO $SLAVE@'$S_IP';"
logfile=`mysql -N -e 'use $DB; SHOW MASTER STATUS' | awk '{print $1}'`
logpos=`mysql -N -e 'use $DB; SHOW MASTER STATUS' | awk '{print $2}'`
mysqldump $DB --lock-all-tables > /tmp/dbdump.db
scp /tmp/dbdump.db root@$S_IP:/tmp
mysql -e "UNLOCK TABLES;"
/sbin/iptables -A INPUT -s $S_IP -p tcp --dport 3306 -j ACCEPT
/etc/init.d/mysqld restart

# slave
mysql tuberosa < /tmp/dbdump.db
mysql -e "
    CHANGE MASTER TO
    MASTER_HOST=$M_IP,
    MASTER_USER=$SLAVE,
    MASTER_LOG_FILE=$logfile,
    MASTER_LOG_POS=$logpos;"
mysql -e "START SLAVE;"