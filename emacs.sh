#!/bin/bashrc

emacs=~/.emacs
emacsd=~/.emacs.d

if emacs --v ; test $? != 0; then
    wget http://ftp.jaist.ac.jp/pub/GNU/emacs/emacs-24.3.tar.gz -O ~/tmp.tar.gz
    mkdir -p ~/tmp && tar xvf ~/tmp.tar.gz -C ~/tmp && cd ~/tmp/*
    ./configure --with-x-toolkit=no --with-xpm=no --with-png=no --with-gif=no
    make
    make install && rm -rf ~/tmp.tar.gz ~/tmp
fi

if test -f ./emacs; then cat ./emacs > $emacs; fi
if test -d $emacsd && test $? != 0 ; then mkdir $emacsd; fi

lisps=(
https://github.com/adolfosousa/ruby-block.el.git
https://github.com/fxbois/web-mode.git
https://github.com/yoshiki/yaml-mode.git
https://github.com/slim-template/emacs-slim.git
https://github.com/defunkt/coffee-mode.git
)

for i in "${lisps[@]}"; do
    file=${i##*/}
    t=${file%.*}
    dir=${t%.*}
    if test -d $emacsd/$dir; test $? != 0; then git clone $i $emacsd/$dir; fi
    if test $? != 0; then wget $i -O $emacsd/$file; fi
done

alias emacs='/usr/local/bin/emacs-24.3'
